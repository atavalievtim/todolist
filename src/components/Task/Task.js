import React from 'react';
import "./Task.scss"

const Task = (props) => {
    return (
        <div className="task_item">
            <input
                type="checkbox"
                onChange={props.taskStatus}

            />
            <span>{props.text}</span>

            <button onClick={props.delete}>Delete</button>
        </div>
    );
};

export default Task;