import React, {useState} from 'react';
import './AddTaskForm.scss'

const AddTaskForm = (props) => {


    return (
        <div className="task_form">
            <input type="text"
                   value={props.value}
                   onChange={props.newTask}
                   placeholder="Create new Task"
            />
            <button onClick={props.addTask}>Add</button>
        </div>
    );
};

export default AddTaskForm;