import './App.css';
import AddTaskForm from "./components/AddTaskForm/AddTaskForm";
import Task from "./components/Task/Task";
import React, {useState} from "react";

function App() {

    const [taskList, setTaskList] = useState([
        {id: 1, text: "Clean house", isDone: false},
        {id: 2, text: "Buy Product", isDone: false},
        {id: 3, text: "Feed dog", isDone: false},
    ])

    const [newTask, setNewTask] = useState('')

    const addNewTask = (event, id) => {
        if(newTask) {
            const newTaskItem = {id:Date.now(), text:newTask, isDone:false}
            setTaskList((taskList) => {
                return [...taskList, newTaskItem]
            })
        }

        setNewTask('')
    }

    const deleteTask = (id) => {
        const index = taskList.findIndex(task => task.id === id);
        const taskListCopy = [...taskList];

        taskListCopy.splice(index, 1)
        setTaskList(taskListCopy)
    }

    const changeTaskStatus = (id) => {

        const index = taskList.findIndex(task => task.id === id);
        const taskListCopy = [...taskList];
        const taskCopy = {...taskList[index]};

        taskCopy.isDone = !taskCopy.isDone;

        setTaskList(taskListCopy)

        console.log(taskCopy.isDone + "  " + id)
    }

    let toDoList = null


    return (
        <div className="App">
            <AddTaskForm
                newTask={event=>setNewTask(event.target.value)}
                value={newTask}
                addTask={addNewTask}

            />
            <>
                {
                    taskList.length > 0
                    ? taskList.map(task => {
                        return <Task key={task.id}
                                     text={task.text}
                                     isDone={task.isDone}
                                     delete={() => deleteTask(task.id)}
                                     taskStatus={() => changeTaskStatus(task.id)}/>
                    })
                    : <div>Nothing TO DO All Task is Done</div>
                }
            </>

        </div>
    );
}

export default App;
